CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides extensions to core migrations functionality by extending
the migrations to be recurring migrations.

 * For a full description of the module visit:
   https://www.drupal.org/project/migrate_recurring_import

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/migrate_recurring_import


REQUIREMENTS
------------

This module requires migrate_tools module.


INSTALLATION
------------

   * Installing the module as you would normally install a contributed Drupal
     module.
     Visit https://www.drupal.org/docs/7/extend/installing-modules for further
     information.


CONFIGURATION
-------------

Configurations can be found at individual edit screens of the migrations.


MAINTAINERS
-----------

 * Heshan Wanigasooriya (heshanlk) - https://www.drupal.org/u/heshanlk

Need help?

Contact me here https://www.heididev.com/hire
